<?php

namespace Gotoroho\TelegramBot\Dto\Request;

readonly class EditMessageReplyMarkup extends AbstractPostJsonRequest
{
    public function __construct(
        public ?int $chat_id = null,
        public ?int $message_id = null,
        public ?string $inline_message_id = null,
        public ?array $reply_markup = null,
    ) {
    }
}
