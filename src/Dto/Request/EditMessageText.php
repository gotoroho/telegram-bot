<?php

namespace Gotoroho\TelegramBot\Dto\Request;

readonly class EditMessageText extends AbstractPostJsonRequest
{
    public function __construct(
        public string $text,
        public ?int $chat_id = null,
        public ?int $message_id = null,
        public ?string $inline_message_id = null,
        public ?string $parse_mode = null,
        public ?array $entities = null,
        public ?bool $disable_web_page_preview = null,
        public ?array $reply_markup = null,
    ) {
    }
}
