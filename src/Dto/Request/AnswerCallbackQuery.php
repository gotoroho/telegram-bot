<?php

namespace Gotoroho\TelegramBot\Dto\Request;

readonly class AnswerCallbackQuery extends AbstractPostJsonRequest
{
    public function __construct(
        public string $callback_query_id,
        public ?string $text = null,
        public ?bool $show_alert = null,
        public ?string $url = null,
        public ?int $cache_time = null,
    ) {
    }
}
