<?php

namespace Gotoroho\TelegramBot\Dto\Request;

readonly class SendMessage extends AbstractPostJsonRequest
{
    public function __construct(
        public int|string $chat_id,
        public string $text,
        public ?int $message_thread_id = null,
        public ?string $parse_mode = null,
        public ?array $entities = null,
        public ?bool $disable_web_page_preview = null,
        public ?bool $disable_notification = null,
        public ?bool $protect_content = null,
        public ?int $reply_to_message_id = null,
        public ?bool $allow_sending_without_reply = null,
        public ?array $reply_markup = null,
    ) {
    }
}
