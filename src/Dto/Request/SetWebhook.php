<?php

namespace Gotoroho\TelegramBot\Dto\Request;

readonly class SetWebhook extends AbstractPostJsonRequest
{
    public function __construct(
        public string $url,
        public string $secret_token,
        public mixed $certificate = null,
        public ?string $ip_address = null,
        public ?int $max_connections = null,
        public ?array $allowed_updates = null,
        public ?bool $drop_pending_updates = null,
    ) {
    }
}
