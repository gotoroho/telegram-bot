<?php

namespace Gotoroho\TelegramBot\Dto\Request;

use Gotoroho\TelegramBot\Enum\HttpMethod;

readonly abstract class AbstractGetRequest extends AbstractRequest
{
    protected function getMethod(): HttpMethod
    {
        return HttpMethod::GET;
    }
}
