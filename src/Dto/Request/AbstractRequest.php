<?php

namespace Gotoroho\TelegramBot\Dto\Request;

use Gotoroho\TelegramBot\Enum\HttpMethod;
use Gotoroho\TelegramBot\TelegramBot;
use GuzzleHttp\Psr7\Request;

readonly abstract class AbstractRequest
{
    public function getHttpClientRequest(string $token): Request
    {
        return new Request(
            $this->getMethod()->name,
            $this->getUrl($token),
            $this->getHeaders(),
            $this->getBody()
        );
    }

    abstract protected function getMethod(): HttpMethod;

    public function getUrl(string $token): string
    {
        return implode('/', [
            TelegramBot::BASE_URI,
            'bot' . $token,
            $this->getFunctionName(),
        ]);
    }

    private function getFunctionName(): string
    {
        $classNameParts = explode('\\', static::class);
        return lcfirst(end($classNameParts));
    }

    protected function getHeaders(): array
    {
        return [];
    }

    protected function getBody(): mixed
    {
        return null;
    }
}
