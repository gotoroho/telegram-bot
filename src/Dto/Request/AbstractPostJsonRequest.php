<?php

namespace Gotoroho\TelegramBot\Dto\Request;

use Gotoroho\TelegramBot\Enum\HttpMethod;
use JsonException;

readonly abstract class AbstractPostJsonRequest extends AbstractRequest
{
    protected function getMethod(): HttpMethod
    {
        return HttpMethod::POST;
    }

    protected function getHeaders(): array
    {
        return ['Content-Type' => 'application/json'];
    }

    /**
     * @throws JsonException
     */
    protected function getBody(): string
    {
        return $this->getBodyFiltered();
    }

    /**
     * @throws JsonException
     */
    protected function getBodyFiltered(): string
    {
        return json_encode(array_filter((array) $this), JSON_THROW_ON_ERROR);
    }
}
