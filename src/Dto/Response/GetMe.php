<?php

namespace Gotoroho\TelegramBot\Dto\Response;

use Gotoroho\TelegramBot\Dto\Type\User;

class GetMe extends AbstractResponse
{
    private ?User $user;

    public function getUser(): ?User
    {
        if ($this->user !== null) {
            return $this->user;
        }
        if (!isset($this->getResponse()['result'])) {
            return null;
        }
        return $this->user = new User(...$this->getResponse()['result']);
    }
}
