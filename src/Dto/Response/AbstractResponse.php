<?php

namespace Gotoroho\TelegramBot\Dto\Response;

use GuzzleHttp\Psr7\Response;
use JsonException;

abstract class AbstractResponse
{
    private array $response;

    /**
     * @throws JsonException
     */
    public function __construct(Response $response)
    {
        $this->response = json_decode(
            $response->getBody(),
            true,
            512,
            JSON_THROW_ON_ERROR,
        );
    }

    public function getResponse(): array
    {
        return $this->response;
    }
}
