<?php

namespace Gotoroho\TelegramBot\Dto\Response;

use Gotoroho\TelegramBot\Dto\Type\Message;

class SendMessage extends AbstractResponse
{
    private ?Message $message;

    public function getMessage(): ?Message
    {
        if ($this->message !== null) {
            return $this->message;
        }
        if (!isset($this->getResponse()['result'])) {
            return null;
        }
        return $this->message = new Message(...$this->getResponse()['result']);
    }
}
