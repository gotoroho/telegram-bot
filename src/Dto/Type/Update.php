<?php

namespace Gotoroho\TelegramBot\Dto\Type;

readonly class Update
{
    public function __construct(
        public int $update_id,
        public ?array $message = null,
        public ?array $edited_message = null,
        public ?array $channel_post = null,
        public ?array $edited_channel_post = null,
        public ?array $inline_query = null,
        public ?array $chosen_inline_result = null,
        public ?array $callback_query = null,
        public ?array $shipping_query = null,
        public ?array $pre_checkout_query = null,
        public ?array $poll = null,
        public ?array $poll_answer = null,
        public ?array $my_chat_member = null,
        public ?array $chat_member = null,
        public ?array $chat_join_request = null,
    ) {
    }

    public function getMessage(): ?Message
    {
        if ($this->message === null) {
            return null;
        }
        return new Message(...$this->message);
    }

    public function getCallbackQuery(): ?CallbackQuery
    {
        if ($this->callback_query === null) {
            return null;
        }
        return new CallbackQuery(...$this->callback_query);
    }
}
