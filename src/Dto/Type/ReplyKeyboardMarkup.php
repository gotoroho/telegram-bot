<?php

namespace Gotoroho\TelegramBot\Dto\Type;

class ReplyKeyboardMarkup
{
    public function __construct(
        public array $keyboard,
        public ?bool $is_persistent = null,
        public ?bool $resize_keyboard = null,
        public ?bool $one_time_keyboard = null,
        public ?string $input_field_placeholder = null,
        public ?bool $selective = null,
    ) {
    }
}
