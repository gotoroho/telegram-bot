<?php

namespace Gotoroho\TelegramBot\Dto\Type;

readonly class Message
{
    public function __construct(
        public int $message_id,
        public int $date,
        public ?int $message_thread_id = null,
        public ?array $from = null,
        public ?array $sender_chat = null,
        public ?array $chat = null,
        public ?array $forward_from = null,
        public ?array $forward_from_chat = null,
        public ?int $forward_from_message_id = null,
        public ?string $forward_signature = null,
        public ?string $forward_sender_name = null,
        public ?int $forward_date = null,
        public ?bool $is_topic_message = null,
        public ?bool $is_automatic_forward = null,
        public ?array $reply_to_message = null,
        public ?array $via_bot = null,
        public ?int $edit_date = null,
        public ?bool $has_protected_content = null,
        public ?string $media_group_id = null,
        public ?string $author_signature = null,
        public ?string $text = null,
        public ?array $entities = null,
        public ?array $animation = null,
        public ?array $audio = null,
        public ?array $document = null,
        public ?array $photo = null,
        public ?array $sticker = null,
        public ?array $video = null,
        public ?array $video_note = null,
        public ?array $voice = null,
        public ?string $caption = null,
        public ?array $caption_entities = null,
        public ?bool $has_media_spoiler = null,
        public ?array $contact = null,
        public ?array $dice = null,
        public ?array $game = null,
        public ?array $poll = null,
        public ?array $venue = null,
        public ?array $location = null,
        public ?array $new_chat_members = null,
        public ?array $left_chat_member = null,
        public ?string $new_chat_title = null,
        public ?array $new_chat_photo = null,
        public ?bool $delete_chat_photo = null,
        public ?bool $group_chat_created = null,
        public ?bool $supergroup_chat_created = null,
        public ?bool $channel_chat_created = null,
        public ?array $message_auto_delete_timer_changed = null,
        public ?int $migrate_to_chat_id = null,
        public ?int $migrate_from_chat_id = null,
        public ?array $pinned_message = null,
        public ?array $invoice = null,
        public ?array $successful_payment = null,
        public ?array $user_shared = null,
        public ?array $chat_shared = null,
        public ?string $connected_website = null,
        public ?array $write_access_allowed = null,
        public ?array $passport_data = null,
        public ?array $proximity_alert_triggered = null,
        public ?array $forum_topic_created = null,
        public ?array $forum_topic_edited = null,
        public ?array $forum_topic_closed = null,
        public ?array $forum_topic_reopened = null,
        public ?array $general_forum_topic_hidden = null,
        public ?array $general_forum_topic_unhidden = null,
        public ?array $video_chat_scheduled = null,
        public ?array $video_chat_started = null,
        public ?array $video_chat_ended = null,
        public ?array $video_chat_participants_invited = null,
        public ?array $web_app_data = null,
        public ?array $reply_markup = null,
    ) {
    }

    public function getChat(): ?Chat
    {
        if ($this->chat === null) {
            return null;
        }
        return new Chat(...$this->chat);
    }
}
