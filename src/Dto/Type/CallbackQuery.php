<?php

namespace Gotoroho\TelegramBot\Dto\Type;

readonly class CallbackQuery
{
    public function __construct(
        public string $id,
        public array $from,
        public string $chat_instance,
        public ?array $message = null,
        public ?string $inline_message_id = null,
        public ?string $data = null,
        public ?string $game_short_name = null,
    ) {
    }

    public function getUser(): User
    {
        return new User(...$this->from);
    }

    public function getMessage(): ?Message
    {
        if ($this->message === null) {
            return null;
        }

        return new Message(...$this->message);
    }
}
