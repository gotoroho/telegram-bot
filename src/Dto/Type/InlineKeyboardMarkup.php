<?php

namespace Gotoroho\TelegramBot\Dto\Type;

class InlineKeyboardMarkup
{
    public function __construct(
        public array $inline_keyboard,
    ) {
    }
}
