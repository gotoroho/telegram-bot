<?php

namespace Gotoroho\TelegramBot\Dto\Type;

class InlineKeyboardButton
{
    public function __construct(
        public string $text,
        public ?string $url = null,
        public ?string $callback_data = null,
        public ?array $web_app = null,
        public ?array $login_url = null,
        public ?string $switch_inline_query = null,
        public ?array $callback_game = null,
        public ?bool $pay = null,
    ) {
    }
}
