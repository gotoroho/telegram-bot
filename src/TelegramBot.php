<?php

namespace Gotoroho\TelegramBot;

use Gotoroho\TelegramBot\Dto\Request\AbstractRequest;
use Gotoroho\TelegramBot\Dto\Request\AnswerCallbackQuery as AnswerCallbackQueryRequest;
use Gotoroho\TelegramBot\Dto\Request\EditMessageText as EditMessageTextRequest;
use Gotoroho\TelegramBot\Dto\Request\EditMessageReplyMarkup as EditMessageReplyMarkupRequest;
use Gotoroho\TelegramBot\Dto\Request\GetMe as GetMeRequest;
use Gotoroho\TelegramBot\Dto\Request\SendMessage as SendMessageRequest;
use Gotoroho\TelegramBot\Dto\Request\SetWebhook as SetWebhookRequest;
use Gotoroho\TelegramBot\Dto\Response\AnswerCallbackQuery as AnswerCallbackQueryResponse;
use Gotoroho\TelegramBot\Dto\Response\EditMessageText as EditMessageTextResponse;
use Gotoroho\TelegramBot\Dto\Response\EditMessageReplyMarkup as EditMessageReplyMarkupResponse;
use Gotoroho\TelegramBot\Dto\Response\GetMe as GetMeResponse;
use Gotoroho\TelegramBot\Dto\Response\SendMessage as SendMessageResponse;
use Gotoroho\TelegramBot\Dto\Response\SetWebhook as SetWebhookResponse;
use GuzzleHttp\Client;
use JsonException;
use Psr\Http\Client\ClientExceptionInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Log\LoggerInterface;

readonly class TelegramBot
{
    public const BASE_URI = 'https://api.telegram.org';

    private Client $httpClient;

    public function __construct(
        private string $token,
        private ?LoggerInterface $logger = null,
    ) {
        $this->httpClient = new Client();
    }

    /**
     * @throws ClientExceptionInterface
     */
    private function sendRequest(AbstractRequest $request): ResponseInterface
    {
        $apiRequest = $request->getHttpClientRequest($this->token);
        $this->logger?->info('Api request', (array) $apiRequest);

        $apiResponse = $this->httpClient->sendRequest($apiRequest);
        $this->logger?->info('Api response', (array) $apiResponse);

        return $apiResponse;
    }

    /**
     * @throws ClientExceptionInterface
     * @throws JsonException
     */
    public function getMe(GetMeRequest $request): GetMeResponse
    {
        return new GetMeResponse($this->sendRequest($request));
    }

    /**
     * @throws ClientExceptionInterface
     * @throws JsonException
     */
    public function setWebhook(SetWebhookRequest $request): SetWebhookResponse
    {
        return new SetWebhookResponse($this->sendRequest($request));
    }

    /**
     * @throws ClientExceptionInterface
     * @throws JsonException
     */
    public function sendMessage(
        SendMessageRequest $request,
    ): SendMessageResponse {
        return new SendMessageResponse($this->sendRequest($request));
    }

    /**
     * @throws ClientExceptionInterface
     * @throws JsonException
     */
    public function answerCallbackQuery(
        AnswerCallbackQueryRequest $request,
    ): AnswerCallbackQueryResponse {
        return new AnswerCallbackQueryResponse($this->sendRequest($request));
    }

    /**
     * @throws ClientExceptionInterface
     * @throws JsonException
     */
    public function editMessageText(
        EditMessageTextRequest $request,
    ): EditMessageTextResponse {
        return new EditMessageTextResponse($this->sendRequest($request));
    }

    /**
     * @throws ClientExceptionInterface
     * @throws JsonException
     */
    public function editMessageReplyMarkup(
        EditMessageReplyMarkupRequest $request,
    ): EditMessageReplyMarkupResponse {
        return new EditMessageReplyMarkupResponse($this->sendRequest($request));
    }
}
