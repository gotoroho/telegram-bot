<?php

namespace Gotoroho\TelegramBot\Enum;

enum HttpMethod
{
    case GET;
    case POST;
}
